# Design

1. Use JS gcloud SDK to set up an authorized frontend to handle token setting
2. Query cloud-shell API's with authentication details
3. Poll cloud-shell environment for ready state
4. Generate link to open in application (pluggable, platform based)

Stretch Goals
1. Config based management
2. Store and read config from local storage
3. Multiple environments
4. Select custom image for runtime

X