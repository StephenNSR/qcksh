## Project Infrastructure

## How to use
The required resources to review and deploy the project are in this directory. If a change is required to the project infrastructure simply make the change and push the code, once it has an approval it will be deployed. If there are no infrastructure changes the site will build and deploy to a named environment matching the branch name and visible in the [merge request]() or [environment]() section of [Gitlab]().

## Forking project set up
The terraform as written relies on a [backend]() with locking to ensure parallel builds don't cause collisions. The deployed resources assume a certain amount of setup with Google Cloud and Cloudflare that is still currently manual. Follow the instructions in [setup](setup/README.md) to get started.


### Required Gitlab Project Configuration
The variables below must be set in the Gitlab project in order for Terraform to successfully run
```
DOMAIN=[domain name managed by cloudflare account]
TF_VAR_project_id=[existing project to attach resources]
BACKEND_CONFIG=[GCS bucket name-masked]
GOOGLE_CREDENTIALS=[Service account JSON as a `file` type]
```

<!-- TODO: Add setup block to pipeline for deploy changes -->

